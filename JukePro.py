# Author: Kevin Wagner
# Date modified: Oct 27th 2014
# Email: kcjwagner@gmail.com
# Copyright: October 2014, Kevin Wagner
# This program is copyrighted material and requires permision prior to use.
# Please email Kevin Wagner at kcjwagner@gmail.com if you would like to use this program.

import wx
import wx.media
import os
import id3reader

# Class for the settings window
class MySettings(wx.Dialog):
  def __init__(self, parent):
    self.parent = parent
    wx.Dialog.__init__(self, parent, -1, 'About', wx.DefaultPosition, wx.Size(400, 200))

    self.SetIcon(wx.Icon('AppIcons\JukePro.ico', wx.BITMAP_TYPE_ICO))
    self.SetBackgroundColour("#EEEEEE")
    vBox = wx.BoxSizer(wx.VERTICAL)
    self.SetSizer(vBox)

    # Show the title
    title = wx.StaticText(self, -1, "Settings", style=wx.ALIGN_CENTRE)
    title.SetFont(wx.Font(20, wx.FONTFAMILY_ROMAN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD))
    vBox.Add(title, 1, wx.EXPAND)

    # Music directory settings
    MusicDirSizer = wx.BoxSizer(wx.HORIZONTAL)
    text1 = wx.StaticText(self, -1, "Music directory: ", style=wx.ALIGN_CENTRE)
    text1.SetFont(wx.Font(10, wx.FONTFAMILY_ROMAN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
    self.MusicDirSettings = wx.TextCtrl(self, -1, MUSIC_DIRECTORY)
    MusicDirSizer.Add(text1, 0)
    MusicDirSizer.Add(self.MusicDirSettings, 1)
    vBox.Add(MusicDirSizer, 1, wx.EXPAND)

    # Photos directory settings
    PhotoDirSizer = wx.BoxSizer(wx.HORIZONTAL)
    text2 = wx.StaticText(self, -1, "Photo directory: ", style=wx.ALIGN_CENTRE)
    text2.SetFont(wx.Font(10, wx.FONTFAMILY_ROMAN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
    self.PhotoDirSettings = wx.TextCtrl(self, -1, PHOTO_DIRECTORY)
    PhotoDirSizer.Add(text2, 0)
    PhotoDirSizer.Add(self.PhotoDirSettings, 1)
    vBox.Add(PhotoDirSizer, 1, wx.EXPAND)

    # Show the ok buttons
    ButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
    okButton = wx.Button(self, 50, "Ok")
    okButton.SetFocus()
    ButtonSizer.Add(okButton, 0)
    ButtonSizer.Add(wx.Button(self, 51, "Cancel"), 0)
    self.Bind(wx.EVT_BUTTON, self.ApplySettings, id=50)
    self.Bind(wx.EVT_BUTTON, self.CancelSettings, id=51)
    vBox.Add(ButtonSizer, 1, wx.ALIGN_CENTRE)

  def ApplySettings(self, event):
    global MUSIC_DIRECTORY
    global PHOTO_DIRECTORY
    global app
    newMusicDir = self.MusicDirSettings.GetValue()
    newPhotoDir = self.PhotoDirSettings.GetValue()

    if not CheckDirExists(newMusicDir):
      dlg = wx.MessageDialog(self, 'The music directory entered is not a valid directory', 'Woops!', wx.OK|wx.ICON_INFORMATION)
      dlg.ShowModal()
      dlg.Destroy()
    elif CheckDirEmpty(newMusicDir, ".mp3"):
      dlg = wx.MessageDialog(self, 'The music directory entered does not contain any .mp3 files', 'Woops!', wx.OK|wx.ICON_INFORMATION)
      dlg.ShowModal()
      dlg.Destroy()
    elif not CheckDirExists(newPhotoDir):
      dlg = wx.MessageDialog(self, 'The photo directory entered is not a valid directory', 'Woops!', wx.OK|wx.ICON_INFORMATION)
      dlg.ShowModal()
      dlg.Destroy()
    elif CheckDirEmpty(newPhotoDir, ".jpg") and CheckDirEmpty(newPhotoDir, ".jpeg"):
      dlg = wx.MessageDialog(self, 'The photo directory entered does not contain any .jpg or .jpeg files', 'Woops!', wx.OK|wx.ICON_INFORMATION)
      dlg.ShowModal()
      dlg.Destroy()
    else:
      SettingsFile = open("Settings.txt", "w")
      SettingsFile.write("MUSIC_DIRECTORY = " + newMusicDir + "\n")
      SettingsFile.write("PHOTO_DIRECTORY = " + newPhotoDir)
      SettingsFile.close()

      self.Destroy()
      if self.parent != None:
        app.resetJukePro()

  def CancelSettings(self, event):
    self.Destroy()

# Class for the menu bar
class MyMenu(wx.MenuBar):
  def __init__(self, parent, callerClass):
    self.parent = parent
    self.callerClass = callerClass
    self.NewDialog = None
    wx.MenuBar.__init__(self)
    parent.SetMenuBar(self)

    # Make the file menu
    self.file = wx.Menu()
    self.file.Append(101, '&Settings', 'Some JukePro settings')
    self.file.Append(102, '&Quit', 'Quit the Application')
    self.Append(self.file, '&File')

    # Make the help menu
    help = wx.Menu()
    help.Append(103, '&Instructions', 'How JukePro works')
    help.Append(104, '&About', 'About JukePro')
    self.Append(help, '&Help')

    # Events
    self.Bind(wx.EVT_MENU, self.OnSettings, id=101)
    self.Bind(wx.EVT_MENU, self.OnQuit, id=102)
    self.Bind(wx.EVT_MENU, self.OnInstructions, id=103)
    self.Bind(wx.EVT_MENU, self.OnAbout, id=104)

  def OnSettings(self, event):
    self.NewDialog = MySettings(self)
    self.NewDialog.ShowModal()

  def OnQuit(self, event):
    self.parent.Destroy()

  def OnInstructions(self, event):
    self.NewDialog = wx.Dialog(None, -1, 'Instructions', wx.DefaultPosition, wx.Size(400, 200))
    self.NewDialog.SetIcon(wx.Icon('AppIcons\JukePro.ico', wx.BITMAP_TYPE_ICO))
    self.NewDialog.SetBackgroundColour("#EEEEEE")
    vBox = wx.BoxSizer(wx.VERTICAL)
    self.NewDialog.SetSizer(vBox)

    title = wx.StaticText(self.NewDialog, -1, "Instructions", style=wx.ALIGN_CENTRE)
    title.SetFont(wx.Font(20, wx.FONTFAMILY_ROMAN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD))
    vBox.Add(title, 0, wx.EXPAND)
    text = wx.StaticText(self.NewDialog, -1, "\nPlease feel free to request a song. When a song is requested it will be played after other songs which have already been requested, but before songs which have not yet been requested. If the song does not move up on the list when requested, this is most likely because it has already been requested. If you have any problems and would like some help, talk to Kevin Wagner", style=wx.ALIGN_CENTRE)
    text.SetFont(wx.Font(10, wx.FONTFAMILY_ROMAN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
    vBox.Add(text, 1, wx.EXPAND)

    self.NewDialog.ShowModal()

  def OnAbout(self, event):
    self.NewDialog = wx.Dialog(None, -1, 'About', wx.DefaultPosition, wx.Size(400, 150))
    self.NewDialog.SetIcon(wx.Icon('AppIcons\JukePro.ico', wx.BITMAP_TYPE_ICO))
    self.NewDialog.SetBackgroundColour("#EEEEEE")
    vBox = wx.BoxSizer(wx.VERTICAL)
    self.NewDialog.SetSizer(vBox)

    title = wx.StaticText(self.NewDialog, -1, "About", style=wx.ALIGN_CENTRE)
    title.SetFont(wx.Font(20, wx.FONTFAMILY_ROMAN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD))
    vBox.Add(title, 0, wx.EXPAND)
    text = wx.StaticText(self.NewDialog, -1, "\nCongradulations and all the best to Mr. and Mrs. Awadalla! This program has been developed expressly for use at Katie and Elie's wedding.\n\nDeveloper: Kevin Wagner\n" + chr(169) + " 2014, Kevin Wagner, kcjwagner@gmail.com", style=wx.ALIGN_CENTRE)
    text.SetFont(wx.Font(10, wx.FONTFAMILY_ROMAN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
    vBox.Add(text, 1, wx.EXPAND)

    self.NewDialog.ShowModal()

  def HideAdminControls(self):
    self.Remove(0)

  def ShowAdminControls(self):
    self.Insert(0, self.file, "File")

# Class for the side panel
class MyMusicList(wx.Panel):
  def __init__(self, parent):
    self.parent = parent
    wx.Panel.__init__(self, parent, -1)
    vBox = wx.BoxSizer(wx.VERTICAL)
    self.SetSizer(vBox)

    # Make the list of songs
    self.Table = wx.ListCtrl(self, -1, style=wx.LC_REPORT)
    self.Table.InsertColumn(0, "Title")
    self.Table.InsertColumn(1, "Artist")
    self.Table.InsertColumn(2, "Order")
    self.Table.InsertColumn(3, "Requested")
    self.songQueue = []
    self.fillQueue(MUSIC_DIRECTORY)
    self.refreshTable()
    vBox.Add(self.Table, 1, wx.EXPAND)

    # Make the buttons
    hBox = wx.BoxSizer(wx.HORIZONTAL)
    self.Down = wx.Button(self, 16, "Down")
    self.Up = wx.Button(self, 17, "Up")
    self.Remove = wx.Button(self, 18, "Remove")
    self.Add = wx.Button(self, 19, "Add")
    hBox.Add(self.Down, 1, wx.EXPAND)
    hBox.Add(self.Up, 1, wx.EXPAND)
    hBox.Add(self.Remove, 1, wx.EXPAND)
    hBox.Add(self.Add, 1, wx.EXPAND)
    vBox.Add(hBox, 0, wx.EXPAND)
    vBox.Add(wx.Button(self, 20, "Request"), 0, wx.EXPAND)

    # Events
    self.Bind(wx.EVT_BUTTON, self.OnDown, id=16)
    self.Bind(wx.EVT_BUTTON, self.OnUp, id=17)
    self.Bind(wx.EVT_BUTTON, self.OnRemove, id=18)
    self.Bind(wx.EVT_BUTTON, self.OnAdd, id=19)
    self.Bind(wx.EVT_BUTTON, self.OnRequest, id=20)

  # Parses directory and fills queue with all mp3 files in the queue
  def fillQueue(self, directory):
    count = 0
    for file in os.listdir(directory):
      if file.endswith(".mp3") or file.endswith(".MP3"):
        id3Tag = id3reader.Reader(directory + "/" + file)
        try:
          title = id3Tag.getValue('title')
          artist = id3Tag.getValue('performer')
        except:
            continue
        self.songQueue.append({"title" : title, "artist" :  artist, "directory" : directory + "/" + file, "requested" : False, "played" : False})
        count = count + 1
      if not "." in file:
		    self.fillQueue(directory + "/" + file)
    print(str(count) + " songs were loaded")

  # Refresh the table on the side to show the contents of the queue
  def refreshTable(self, hidePlayed=False):
    self.Table.DeleteAllItems()
    count = 0
    for song in self.songQueue:
      # Add the song to the table
      if song["played"] and hidePlayed:
        continue
      elif song["played"] and not hidePlayed:
        count += 1
        if song["requested"]:
          self.Table.Append([song["title"], song["artist"], count, "Requested"])
        else:
          self.Table.Append([song["title"], song["artist"], count, ""])
        self.Table.SetItemTextColour(count - 1, "#aaaaaa")
      else:
        count += 1
        if song["requested"]:
          self.Table.Append([song["title"], song["artist"], count, "Requested"])
        else:
          self.Table.Append([song["title"], song["artist"], count, ""])

  # Error handling for cases when the user does not have exactly one song selected
  def MakeSureOnlyOneSelected(self):
    if (self.Table.GetSelectedItemCount() > 1):
      dlg = wx.MessageDialog(self, 'Make sure you are only selecting one song at a time', 'Woops!', wx.OK|wx.ICON_INFORMATION)
      dlg.ShowModal()
      dlg.Destroy()
    elif (self.Table.GetSelectedItemCount() == 0):
      dlg = wx.MessageDialog(self, 'Make sure you select at least one song', 'Woops!', wx.OK|wx.ICON_INFORMATION)
      dlg.ShowModal()
      dlg.Destroy()
    else:
      return True
    return False

  # Event handler for when the down button is pressed
  def OnDown(self, event):
    if self.MakeSureOnlyOneSelected():
      # Find the selected item
      for count in range(0, len(self.songQueue) - 1):
        if self.Table.IsSelected(count) and not (not self.songQueue[count]["played"] and self.songQueue[count+1]["played"]):
          song = self.songQueue.pop(count)
          self.songQueue.insert(count+1, song)
          self.refreshTable()
          break
        elif self.Table.IsSelected(count) and not self.songQueue[count]["played"] and self.songQueue[count+1]["played"]:
          dlg = wx.MessageDialog(self, 'Songs that have not yet been played can not be moved bellow songs that have already been played.', 'Woops!', wx.OK|wx.ICON_INFORMATION)
          dlg.ShowModal()
          dlg.Destroy()

  # Event handler for when the up button is pressed
  def OnUp(self, event):
    if self.MakeSureOnlyOneSelected():
      # Find the selected item
      for count in range(1, len(self.songQueue)):
        if self.Table.IsSelected(count) and not (self.songQueue[count]["played"] and not self.songQueue[count-1]["played"]):
          song = self.songQueue.pop(count)
          self.songQueue.insert(count-1, song)
          self.refreshTable()
          break
        elif self.Table.IsSelected(count) and self.songQueue[count]["played"] and not self.songQueue[count-1]["played"]:
          dlg = wx.MessageDialog(self, 'Songs that have already been played can not be moved above songs that have not yet been played. You must first add the song back into the queue before moving it up in the queue.', 'Woops!', wx.OK|wx.ICON_INFORMATION)
          dlg.ShowModal()
          dlg.Destroy()

  # Event handler for when the remove song button is pressed
  def OnRemove(self, event):
    if self.MakeSureOnlyOneSelected():
      # Find the selected item
      for count in range(0, len(self.songQueue)):
        if self.Table.IsSelected(count) and not self.songQueue[count]["played"]:
          song = self.songQueue.pop(count)
          song["played"] = True
          self.songQueue.append(song)
          self.refreshTable()
          break

  # Event handler for when the add song button is pressed
  def OnAdd(self, event):
    if self.MakeSureOnlyOneSelected():
      # Find the selected item
      for count in range(0, len(self.songQueue)):
        if self.Table.IsSelected(count) and self.songQueue[count]["played"]:
          song = self.songQueue.pop(count)
          song["played"] = False
          song["requested"] = False

          # Figure out where to move the selected item to
          for count in range(0, len(self.songQueue)):
            if self.songQueue[count]["played"]:
              self.songQueue.insert(count, song)
              break
            if count == len(self.songQueue) - 1:
              self.songQueue.append(song)
              break
          
          self.refreshTable()
          break

  # Event handler for when request button is pressed
  def OnRequest(self, event):
    if self.MakeSureOnlyOneSelected():
      # Find the selected item
      for count in range(0, len(self.songQueue)):
        if self.Table.IsSelected(count):
          # Double check that they meant to do this using a dialog box
          dlg = wx.MessageDialog(self, "Would you like to request '" + self.songQueue[count]["title"] + "' by " + self.songQueue[count]["artist"], "caption", wx.YES_NO | wx.ICON_QUESTION)
          result = dlg.ShowModal() == wx.ID_YES
          dlg.Destroy()
          if not result:
            break

          song = self.songQueue.pop(count)
          song["requested"] = True

          # Figure out where to move the selected item to
          for count in range(0, len(self.songQueue)):
            if not self.songQueue[count]["requested"]:
              self.songQueue.insert(count, song)
              break
          
          self.refreshTable()
          break

  # Return the next song to be played and add it to the back of the queue marked as 'played'
  def getNext(self):
    nextSong = self.songQueue.pop(0)
    nextSong["played"] = True
    self.songQueue.append(nextSong)
    self.refreshTable()
    return nextSong

  # Return the previous song that was played and add the current song to the front of the queue marked as 'not played'
  def getPrev(self):
    currentSong = self.songQueue.pop(len(self.songQueue) - 1)
    currentSong["played"] = False
    self.songQueue.insert(0, currentSong)
    self.refreshTable()
    return self.songQueue[len(self.songQueue) - 1]

  def HideAdminControls(self):
    # Hide the admin buttons
    self.Down.Hide()
    self.Up.Hide()
    self.Remove.Hide()
    self.Add.Hide()

    # Hide played songs
    self.refreshTable(True)

    # Refresh layout
    self.Layout()

  def ShowAdminControls(self):
    # Show the admin buttons
    self.Down.Show()
    self.Up.Show()
    self.Remove.Show()
    self.Add.Show()

    # Hide played songs
    self.refreshTable()

    # Refresh layout
    self.parent.Layout()

# Class for the music player
class MyMusicPlayer(wx.Panel):
  def __init__(self, parent, musicList):
    self.musicList = musicList
    self.parent = parent
    wx.Panel.__init__(self, parent, -1)
    vBox = wx.BoxSizer(wx.VERTICAL)
    self.SetSizer(vBox)

    # When set to true, the media player will pause when the song ends
    self.PauseOnNext = False

    # Media player
    self.MediaPlayer = wx.media.MediaCtrl(self, szBackend=wx.media.MEDIABACKEND_WMP10)
    self.MediaPlayer.Load(self.musicList.getNext()["directory"])

    # Current song text
    self.currentSong = wx.StaticText(self, -1, "", style=wx.ALIGN_CENTRE)
    self.currentSong.SetFont(wx.Font(20, wx.FONTFAMILY_ROMAN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD))
    vBox.Add(self.currentSong, 0, wx.EXPAND)

    # Slider
    ProgressSizer = wx.BoxSizer(wx.HORIZONTAL)
    self.currentTime = wx.StaticText(self, -1, " 0:00", (30,15), style=wx.ALIGN_CENTRE)
    self.slider = wx.Slider(self, -1, 0, 0, 1000)
    self.endTime = wx.StaticText(self, -1, "0:00 ", (30,15), style=wx.ALIGN_CENTRE)
    ProgressSizer.Add(self.currentTime)
    ProgressSizer.Add(self.slider, 1, wx.EXPAND)
    ProgressSizer.Add(self.endTime)
    vBox.Add(ProgressSizer, 0, wx.EXPAND)

    # Buttons
    ControlsSizer = wx.BoxSizer(wx.HORIZONTAL)
    noLog = wx.LogNull()
    self.SkipBack = wx.BitmapButton(self, 1, wx.Bitmap('AppIcons\Small-Media-Controls-Skip-To-Start-icon.png'))
    self.Play = wx.BitmapButton(self, 2, wx.Bitmap('AppIcons\Small-Media-Controls-Play-icon.png'))
    self.Play.Enable()
    self.Pause = wx.BitmapButton(self, 3, wx.Bitmap('AppIcons\Small-Media-Controls-Pause-icon.png'))
    self.Pause.Disable()
    self.SkipForward = wx.BitmapButton(self, 4, wx.Bitmap('AppIcons\Small-Media-Controls-End-icon.png'))
    del noLog
    ControlsSizer.Add(self.SkipBack, 1, wx.EXPAND)
    ControlsSizer.Add(self.Play, 1, wx.EXPAND)
    ControlsSizer.Add(self.Pause, 1, wx.EXPAND)
    ControlsSizer.Add(self.SkipForward, 1, wx.EXPAND)

    # Volume control
    VolumeSizer = wx.BoxSizer(wx.VERTICAL)
    self.VolSlider = wx.Slider(self, -1, 0, 0, 100, style=wx.SL_VERTICAL)
    self.HighVol = wx.StaticBitmap(self, -1, wx.Bitmap('AppIcons\ExtraSmall-Media-Controls-High-Volume-icon.png'))
    VolumeSizer.Add(self.VolSlider)
    VolumeSizer.Add(self.HighVol, 0, wx.EXPAND)
    ControlsSizer.Add(VolumeSizer, 0, wx.EXPAND)
    vBox.Add(ControlsSizer, 0, wx.EXPAND)

    # Events
    self.Bind(wx.media.EVT_MEDIA_LOADED, self.SongLoaded)
    self.Bind(wx.EVT_SLIDER, self.OnSeek, self.slider)
    self.Bind(wx.EVT_SLIDER, self.OnVolSeek, self.VolSlider)
    self.Bind(wx.EVT_BUTTON, self.OnBack, id=1)
    self.Bind(wx.EVT_BUTTON, self.OnPlay, id=2)
    self.Bind(wx.EVT_BUTTON, self.OnPause, id=3)
    self.Bind(wx.EVT_BUTTON, self.OnForward, id=4)

  # Event handler for when a song is loaded
  def SongLoaded(self, event):
    # Set the timer event which may have been removed by next or back events
    self.timer = wx.Timer(self)
    self.Bind(wx.EVT_TIMER, self.OnTimer)
    self.timer.Start(100)

    # Set the current song text
    self.currentSong.SetLabel(self.musicList.songQueue[-1]["title"] + " by " + self.musicList.songQueue[-1]["artist"])

    # Initialise the progress bar and progress text
    self.slider.SetRange(0, self.MediaPlayer.Length())
    TotalSeconds = self.MediaPlayer.Length()/1000
    Minutes = str(TotalSeconds/60)
    if (TotalSeconds%60 < 10):
      Seconds = "0" + str(TotalSeconds%60)
    else:
      Seconds = str(TotalSeconds%60)
    self.endTime.SetLabel(" " + Minutes + ":" + Seconds + " ")

    # If play button is pressed play the music right away
    if self.Pause.IsEnabled():
      self.MediaPlayer.Play()
    if self.PauseOnNext:
      self.MediaPlayer.Pause()
      self.Pause.Disable()
      self.Play.Enable()
      self.PauseOnNext = False

    # Refresh the layout
    self.Layout()

  # Event handler for the timer (every second)
  def OnTimer(self, event):
    # Update the progress bar and progress text
    offset = self.MediaPlayer.Tell()
    TotalSeconds = offset/1000
    Minutes = str(TotalSeconds/60)
    if (TotalSeconds%60 < 10):
      Seconds = "0" + str(TotalSeconds%60)
    else:
      Seconds = str(TotalSeconds%60)
    self.currentTime.SetLabel(" " + Minutes + ":" + Seconds + " ")
    self.slider.SetValue(offset)

    # If the media player is stopped and the play button is pressed
    # then we must have reached the end of the song, so we should get
    # the next song
    if self.MediaPlayer.GetState() == 0 and self.Pause.IsEnabled():
      self.OnForward(None)

  # Event handler for when the slider is used to change progress through song
  def OnSeek(self, event):
    # Update the progress text
    offset = self.slider.GetValue()
    TotalSeconds = offset/1000
    Minutes = str(TotalSeconds/60)
    if (TotalSeconds%60 < 10):
      Seconds = "0" + str(TotalSeconds%60)
    else:
      Seconds = str(TotalSeconds%60)
    self.currentTime.SetLabel(" " + Minutes + ":" + Seconds + " ")

    # Move the media player to the correct position in song
    self.MediaPlayer.Seek(offset)

  # Event handler for when the volume is changed
  def OnVolSeek(self, event):
    sliderVal = float(100 - self.VolSlider.GetValue())/100
    self.MediaPlayer.SetVolume(sliderVal)

  # Event handler for when the play button is pressed
  def OnPlay(self, event):
    self.Play.Disable()
    self.Pause.Enable()
    self.MediaPlayer.Play()

  # Event handler for when the pause button is pressed
  def OnPause(self, event):
    self.Play.Enable()
    self.Pause.Disable()
    self.MediaPlayer.Pause()

  # Event handler for when the back button is pressed
  def OnBack(self, event):
    # If we're more than 2 sec into song, restart it
    # If there is a previous song, play that
    # If the last song in the list is no longer the song being played, play that
    if(self.MediaPlayer.Tell()/1000 > 2):
      self.MediaPlayer.Seek(0)
    elif(self.musicList.songQueue[-2]["played"]):
      # Unbind the timer, or else we risk OnForward being called
      # while we are switching to previous song
      self.Unbind(wx.EVT_TIMER)

      # Restart current song
      self.MediaPlayer.Load(self.musicList.getPrev()["directory"])
    else:
      # Unbind the timer, or else we risk OnForward being called
      # while we are switching to previous song
      self.Unbind(wx.EVT_TIMER)

      # Load current song
      self.MediaPlayer.Load(self.musicList.songQueue[-1]["directory"])

  def OnForward(self, event):
    if(self.musicList.songQueue[0]["played"]):
      self.OnPause(None)
    else:
      # Unbind the timer, or else we risk OnForward being called
      # while we are switching to next song
      self.Unbind(wx.EVT_TIMER)

      # Load next song
      nextSong = self.musicList.getNext()
      self.MediaPlayer.Load(nextSong["directory"])

  def HideAdminControls(self):
    # Hide the admin buttons
    self.SkipBack.Hide()
    self.Play.Hide()
    self.Pause.Hide()
    self.SkipForward.Hide()
    self.VolSlider.Hide()
    self.HighVol.Hide()

    # Disable the progress slider
    self.slider.Disable()

    # Refresh layout
    self.parent.Layout()

  def ShowAdminControls(self):
    # Show the admin buttons
    self.SkipBack.Show()
    self.Play.Show()
    self.Pause.Show()
    self.SkipForward.Show()
    self.VolSlider.Show()
    self.HighVol.Show()

    # Enable the progress slider
    self.slider.Enable()

    # Refresh layout
    self.parent.Layout()


# Class the slideshow window
class MyPhotos(wx.Panel):
  def __init__(self, parent):
    wx.Panel.__init__(self, parent, -1)
    self.slideshowSizer = wx.BoxSizer(wx.VERTICAL)
    self.SetSizer(self.slideshowSizer)

    # Get a list of photos
    self.photos = []
    self.currentPhotoIndex = 0
    self.fillSLideShow(PHOTO_DIRECTORY)

    # Add the first picture
    width = self.GetSize()[0]
    height = self.GetSize()[1]
    image = self.photos[self.currentPhotoIndex].Scale(width, height, wx.IMAGE_QUALITY_HIGH)
    self.Bitmap = wx.StaticBitmap(self, -1, image.ConvertToBitmap())
    self.slideshowSizer.Add(self.Bitmap, 1, wx.EXPAND)

    # Set the timer event which may have been removed by next or back events
    self.timer = wx.Timer(self)
    self.Bind(wx.EVT_TIMER, self.OnTimer)
    self.timer.Start(5000)

    self.Bind(wx.EVT_SIZE , self.onResize)

  # Parses directory and fills queue with all mp3 files in the queue
  def fillSLideShow(self, directory):
    for file in os.listdir(directory):
      if file.endswith(".jpg") or file.endswith(".jpeg"):
        self.photos.append(wx.Image(directory + '/' + file, wx.BITMAP_TYPE_JPEG))
      if not "." in file:
        self.fillSLideShow(directory + "/" + file)

  def OnTimer(self, event):
    oldPhotoIndex = self.currentPhotoIndex
    self.currentPhotoIndex += 1
    if self.currentPhotoIndex >= len(self.photos):
      self.currentPhotoIndex = self.currentPhotoIndex % len(self.photos)

    width = self.GetSize()[0]
    height = self.GetSize()[1]
    newImage = self.photos[self.currentPhotoIndex].Scale(width, height, wx.IMAGE_QUALITY_HIGH)
    self.Bitmap.SetBitmap(newImage.ConvertToBitmap())

  def onResize(self, event):
    width = self.GetSize()[0]
    height = self.GetSize()[1]
    self.Bitmap.Destroy()
    image = self.photos[self.currentPhotoIndex].Scale(width, height, wx.IMAGE_QUALITY_HIGH)
    self.Bitmap = wx.StaticBitmap(self, -1, image.ConvertToBitmap())
    self.slideshowSizer.Add(self.Bitmap, 1, wx.EXPAND)

    self.Layout()

 
class MyApp(wx.App):
  def OnInit(self):
    self.ReadSettings()
    self.InitialiseSettings()

    # Set up the parent frame
    self.JukeFrame =  wx.Frame(None, -1, 'JukePro', wx.DefaultPosition, wx.Size(800, 600))
    self.JukeFrame.SetIcon(wx.Icon('AppIcons\JukePro.ico', wx.BITMAP_TYPE_ICO))
    hBox = wx.BoxSizer(wx.HORIZONTAL)
    vBox = wx.BoxSizer(wx.VERTICAL)
    self.JukeFrame.SetSizer(hBox)

    # Set up the menubar
    self.menuBar = MyMenu(self.JukeFrame, self)

    # Set up the music list
    self.musicList = MyMusicList(self.JukeFrame)
    hBox.Add(self.musicList, 2, wx.EXPAND)

    # Set up the slideshow
    self.slideshow = MyPhotos(self.JukeFrame)
    vBox.Add(self.slideshow, 1, wx.EXPAND)

    # Set up the music player
    self.musicPlayer = MyMusicPlayer(self.JukeFrame, self.musicList)
    vBox.Add(self.musicPlayer, 0, wx.EXPAND)
    hBox.Add(vBox, 5, wx.EXPAND)

    self.pressedKeys = []
    self.adminMode = True
    self.Bind(wx.EVT_KEY_DOWN, self.keyDown)
    self.Bind(wx.EVT_KEY_UP, self.keyUp)

    self.JukeFrame.ShowFullScreen(True, wx.FULLSCREEN_NOBORDER)
    return True

  def ReadSettings(self):
    global MUSIC_DIRECTORY
    global PHOTO_DIRECTORY

    # If the settings file doesnt exist
    if not os.path.isfile("Settings.txt"):
      SettingsFile = open("Settings.txt", "w")
      SettingsFile.write("MUSIC_DIRECTORY = \n")
      SettingsFile.write("PHOTO_DIRECTORY = ")
      SettingsFile.close()
    
    # Read the settings from the settings file
    SettingsFile = open('Settings.txt', 'r')
    line = SettingsFile.readline()
    while(len(line) > 0):
      left = line[0:line.index(" = ")]
      right = line[line.index(" = ") + 3:len(line)].replace("\n", "")
      if left == "MUSIC_DIRECTORY":
        MUSIC_DIRECTORY = right
      elif left == "PHOTO_DIRECTORY":
        PHOTO_DIRECTORY = right

      line = SettingsFile.readline()
    SettingsFile.close()

  def InitialiseSettings(self):
    global MUSIC_DIRECTORY
    global PHOTO_DIRECTORY

    # Make sure the settings are valid
    if (not CheckDirExists(MUSIC_DIRECTORY)) or (CheckDirEmpty(MUSIC_DIRECTORY, ".mp3")) or (not CheckDirExists(PHOTO_DIRECTORY)) or (CheckDirEmpty(PHOTO_DIRECTORY, ".jpg") and CheckDirEmpty(PHOTO_DIRECTORY, ".jpeg")):
      dlg = wx.MessageDialog(None, 'Please take a moment to initialise JukePro.', 'Woops!', wx.OK|wx.ICON_INFORMATION)
      dlg.ShowModal()
      dlg.Destroy()
      settings = MySettings(None)
      settings.ShowModal()
      self.ReadSettings()

  def keyDown(self, event):
    if self.menuBar.NewDialog != None:
      event.Skip()
      return

    key = event.GetKeyCode()
    if not key in self.pressedKeys:
      self.pressedKeys.append(key)

  def keyUp(self, event):
    if self.menuBar.NewDialog != None:
      event.Skip()
      return

    if len(self.pressedKeys) == 2 and 308 in self.pressedKeys and 84 in self.pressedKeys:
      self.toggleGracefullPause()
    if len(self.pressedKeys) == 2 and 308 in self.pressedKeys and 65 in self.pressedKeys:
      self.toggleAdminMode()
    key = event.GetKeyCode()
    if key in self.pressedKeys:
      self.pressedKeys.remove(key)

  def toggleGracefullPause(self):
    if self.musicPlayer.PauseOnNext:
      self.musicPlayer.PauseOnNext = False
      dlg = wx.MessageDialog(None, 'JukePro will keep playing when song ends', 'Admin Command', wx.OK|wx.ICON_INFORMATION)
    else:
      self.musicPlayer.PauseOnNext = True
      dlg = wx.MessageDialog(None, 'JukePro will pause once song ends', 'Admin Command', wx.OK|wx.ICON_INFORMATION)
    dlg.ShowModal()
    dlg.Destroy()

  def toggleAdminMode(self):
    if self.adminMode:
      self.musicPlayer.HideAdminControls()
      self.musicList.HideAdminControls()
      self.menuBar.HideAdminControls()
      self.adminMode = False
    else:
      self.musicPlayer.ShowAdminControls()
      self.musicList.ShowAdminControls()
      self.menuBar.ShowAdminControls()
      self.adminMode = True

  def resetJukePro(self):
    self.JukeFrame.Destroy()
    self.OnInit()

def CheckDirExists(directory):
  if os.path.isdir(directory):
    return True
  else:
    return False

def CheckDirEmpty(directory, fileType):
  for file in os.listdir(directory):
    if file.endswith(fileType):
      return False
    elif not "." in file:
      if CheckDirEmpty(directory + "/" + file, fileType):
        return False
  return True
 
app = MyApp(0)
app.MainLoop()